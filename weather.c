/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "fdserial.h"
#include "adcDCpropab.h"
#include <propeller.h>
#include <math.h>                   // simpletools header

#define SCL_Pin               15
#define SDA_Pin               14


#define RTC_Addr              0x68            
                                                  
#define RTC_Read              ((RTC_Addr << 1) | 0x01) 
#define RTC_Write             ((RTC_Addr << 1) & 0xFE) 


#define TEMP_MSB              0x11 
#define TEMP_LSB              0x12 




  i2c *dsBus;                    // i2c bus ID
  int secs = 0; 
  int mins = 0;
  int hrs  = 0;
  int am_pm = 0;
  float temp = 0;
  int day  = 0;
  int date = 0;
  int month = 0;
  int year = 0;

static int dht_timeout[32];
static int dht_last_temp, dht_last_humidity, dht_timeout_ignore;



/* Function Declarations*/
int bcd_to_dec(int d);                                
int dec_to_bcd(int d) ;
void DS3231_Write(int address, int reg, int value);    
int DS3231_Read(int address, int reg); 

void check_Date_Time();                                                  
void set_Date_Time();
void read_Date_Time();

float WaterLevel();
int flame();
int dht22_getTemp() ;
int dht22_getHumidity();
char dht22_read(int dht_pin);
  
int main()                                    // Main function
{
  fdserial *blue;
  // Add startup code here.
  blue = fdserial_open(9, 8, 0, 9600);
  dsBus = i2c_open(dsBus, SCL_Pin, SDA_Pin,   0); 
  adc_init(21,20,19,18);
  char data;
  char datam = "d";
  int count = 0;
  int water;
  int flameVal;
  int luxVal;
  int temp;
  int humid;
   //print("im here 1\n");
  while(1)
  {
    //print("im here 2\n");
     //writeChar(blue, CLS);
     data = fdserial_rxChar(blue);   
     if(data != -1){
       print("command received\n");
       //print("%c ", data);
       //print("%x", data);
     }     
     
     if(data == 0x64){
      //print("im here 3\n");
      writeChar(blue, CLS);
      //dprint(blue,"hello\n");
       while(i2c_busy(dsBus, RTC_Addr));
                            // ensure i2c bus not busy
       check_Date_Time();
       
       water = WaterLevel();
        
       flameVal = flame();
        
       luxVal = Lux();
       
       temp = temperature();
        
       humid = humidity();
       dprint(blue, "{water:\"%d\",flame:%d,lux:%d,temp:%d,humidity:%d,date:\"%d/%d/20%d/\",time:\"%02d\\:%02d\\:%02d\"}\n", water, flameVal, luxVal, temp, humid, month, date, year, hrs, mins, secs); 
       print("data sent\n"); 
       /*
       print("water ->%d\n", water);
       print("flame ->%f\n",flameVal);
       print("lux ->%d\n", luxVal);
       print("temp ->%d\n", temp);
       print("humidity ->%d\n", humid);
       print("date %d/%d/20%d \n", month, date, year);
       print("Time = %02d:%02d:%02d ", hrs, mins, secs);
        */
      
     }    
               
             
  }  
 
}  

float WaterLevel()                                    // Main function
{
  
    float v2;
    v2 = adc_volts(2);
    v2 = (v2 - 0.75) * 5.0;                  // Check A/D 2 
    if (v2 < 0.0){
      v2 = 0.0;
    }   
    return v2;
   
}

int flame()                                    // Main function
{                                              //if returned int is less than 30 there is a fire
   
  float v2;
  
 
    int fire = (adc_volts(3) * 100);
    //putChar(HOME);
    
  	return fire;
   //}
 } 



int Lux()
{
	float voltage;
		voltage = adc_volts(3);
		float volts = 1/voltage;
		float resistance = volts/.000520;
		float lux = 12500000 * pow(resistance,-1.4059);
		return lux;
		
}

int temperature()
{
	int c = dht22_read(2);
	int t = dht22_getTemp();
	return t;
	
}

int humidity()
{
	int c = dht22_read(2);
	int h = dht22_getHumidity();
	return h;
	
}

int dht22_getTemp() 
{
  int temp = dht_last_temp;
  temp = temp * 9 / 5 + 320;
  return temp;
}


int dht22_getHumidity() 
{  
  return dht_last_humidity;
}


char dht22_read(int dht_pin) 
{
  if(dht_pin >= 0 && dht_pin < 32) 
  { 
    int dhtpm = (1 << dht_pin);
    if (CNT - dht_timeout[dht_pin] < CLKFREQ/2 && dht_timeout[dht_pin] != 0 && !(dht_timeout_ignore & dhtpm))
      waitcnt(dht_timeout[dht_pin] + CLKFREQ/2);  
    int dhtp[44];
    int dhtc = 0, dhtk = 0, dhts = 0, dhto = 0, dhth = 0, dhtt = 0, dhte = 0;
    OUTA &= ~dhtpm;
    DIRA |= dhtpm;
    
    //waitcnt(CLKFREQ/55 + CNT);  
    DIRA &= ~dhtpm;
    dhto = CNT;
    int dhst = 0;
    for (int j = 0; j < CLKFREQ / 2000; j++) 
    {
      if (!(INA & dhtpm) && !dhst)
        dhst = 1;
      if ((INA & dhtpm) && dhst) 
      {
        dhst = 0;
        dhtp[dhtc] = CNT;
        if (dhtc < 44)
          dhtc++;   
      }
    }
    for (dhtc = 2; dhtc < 18; dhtc++) 
    {
      dhth <<= 1;
      dhth |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    for (dhtc = 18; dhtc < 34; dhtc++) 
    {
      dhtt <<= 1;
      dhtt |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    for (dhtc = 34; dhtc < 42; dhtc++) 
    {
      dhtk <<= 1;
      dhtk |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    
    dhts = dhtt & 32768;
    dhtt &= 32767;
    dhtt *= (dhts ? -1 : 1);
    dhte = (((dhth >> 8) & 255) + (dhth & 255) + ((dhtt >> 8) & 255) + (dhtt & 255)) & 255;
    dht_last_humidity = dhth;
    dht_last_temp = dhtt;
    dht_timeout[dht_pin] = CNT;
    return dhte == dhtk;
    
  } 
  else
    return 0;    
}




void check_Date_Time()
{
  read_Date_Time();
  if (year < 1){
    //print("Setting Date & Time...");
    //pause(500); 
    set_Date_Time();
  }
}


void set_Date_Time(int hr_10, int hr_1, int yr_10, int yr_1)
{  


  print("\nEnter year(YY): ");
  scan( " %d", &year);  
  yr_10 = year / 10;
  yr_10 <<= 4;
  yr_1 = year % 10;
  year = yr_10 | yr_1;
  
  print("\nEnter Month(1-12): ");
  scan( " %d", &month);


  print("\nEnter Day(1-7 [Sunday = 1]): ");
  scan( " %d", &day);


  print("\nEnter Date(1-31): ");
  scan( " %d", &date);


  print("\nEnter Hour(1-12): ");
  scan( " %d", &hrs);
  hr_10 = hrs / 10;
  hr_10 <<= 4;
  hr_1 = hrs % 10;
  hrs = hr_10 | hr_1;


  print("\nEnter 1 for AM or 2 for PM: ");
  scan( " %d", &am_pm);


// code for am_pm status
  switch(am_pm)
  {
     case 1:  //AM
     {
      hrs = (0x40 | (0x1F & hrs));
      am_pm = 0b100;
      break;
     }
     case 2:  //PM
     {
      hrs = (0x60 | (0x1F & hrs));
      am_pm = 0b110;
      break;


     } 
   }                 
       
      
  
  print("\nEnter Minute(1-59): ");
  scan( " %d", &mins);
  print("\nEnter Second(1-59): ");
  scan( " %d", &secs);
                                                        
  DS3231_Write(dsBus, 0x06, year);//set year 
  DS3231_Write(dsBus, 0x05, dec_to_bcd(month));//set month 
  DS3231_Write(dsBus, 0x03, dec_to_bcd(day));//set day 
  DS3231_Write(dsBus, 0x04, dec_to_bcd(date));//set date 
  DS3231_Write(dsBus, 0x02, hrs);//set hour 
  DS3231_Write(dsBus, 0x01, dec_to_bcd(mins));//set minutes 
  DS3231_Write(dsBus, 0x00, dec_to_bcd(secs));//set seconds 
  
}  


void read_Date_Time()
{
  secs = bcd_to_dec(DS3231_Read(dsBus, 0x00));
  mins = bcd_to_dec(DS3231_Read(dsBus, 0x01));
  hrs = DS3231_Read(dsBus, 0x02);
  am_pm = hrs;
  am_pm >>= 4;
  hrs = bcd_to_dec(0x1F & hrs); 
  day = bcd_to_dec(DS3231_Read(dsBus, 0x03));
  date = bcd_to_dec(DS3231_Read(dsBus, 0x04));
  month = bcd_to_dec(DS3231_Read(dsBus, 0x05));
  year = bcd_to_dec(DS3231_Read(dsBus, 0x06));


}    




int bcd_to_dec(int d)                
{                                                                                          
         return ((d & 0x0F) + (((d & 0xF0) >> 4) * 10)); 
}                                
                                                              


int dec_to_bcd(int d) 
{ 
         return (((d / 10) << 4) & 0xF0) | ((d % 10) & 0x0F); 
} 




void DS3231_Write(int address, int reg, int value)    
{  
         i2c_start(dsBus);                  
         i2c_writeByte(dsBus, RTC_Write); 
         i2c_writeByte(dsBus, reg); 
         i2c_writeByte(dsBus, value);    
         i2c_stop(dsBus); 
}  


int DS3231_Read(int address, int reg) 
{                                      
         int value = 0; 
         i2c_start(dsBus);                                                      
         i2c_writeByte(dsBus, RTC_Write); 
         i2c_writeByte(dsBus, reg); 
         i2c_start(dsBus);                                                      
         i2c_writeByte(dsBus, RTC_Read); 
         value = i2c_readByte(dsBus, 1);                      
         i2c_stop(dsBus);                  
         return value; 
} 

