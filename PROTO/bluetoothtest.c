/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "fdserial.h"
int main()                                    // Main function
{
  
  fdserial *blue;

  blue = fdserial_open(9, 8, 0, 9600);
  // Add startup code here.
  char c;
 
  while(1)
  {
    writeChar(blue, CLS);
    dprint(blue, "Click this terminal, \n");
    dprint(blue, "and type on keyboard...\n\n");
    c = fdserial_rxChar(blue);
    if(c != -1)
    {
      dprint(blue, "You typed: %c\n", c);
      print("%c", c);
    }
         
   
  } 
}
