#include "simpletools.h"
#include "adcDCpropab.h"
#include <propeller.h>
#include <math.h>
int main() 
{
  adc_init(21, 20, 19, 18);
  float voltage;
  while(1) 
  {
    voltage = adc_volts(3);
    float volts = 1/voltage;
    float resistance = volts/.000520;
    float lux = 12500000 * pow(resistance,-1.4059);
    print("Volts = %f V, Lux = %f Lux\n",voltage, lux);
    pause(250);
    int t, h, c;
      c = dht22_read(2);
      t = dht22_getTemp();
      h = dht22_getHumidity();
      
      print("check = %d, temperature = %.1f*F, humidity = %.1f\r", c, t / 10.0, h / 10.0);
      pause(250);
      
  } 
} 
