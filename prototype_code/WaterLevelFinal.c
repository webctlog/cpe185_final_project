#include "simpletools.h"                      // Include simple tools
#include "adcDCpropab.h"                      // Include adcDCpropab

float WaterLevel(float v2)                                    // Main function
{
  //char waterlevel[13];
  //char waterlevelend[10];
  //char waterlevelendend[6];
  //char waterlevelall[29];
  
    v2 = (v2 - 0.75) * 5.0;                  // Check A/D 2 
    if (v2 < 0.0)
      v2 = 0.0;
    //putChar(HOME);                             // Cursor -> top-left "home"
    //print("Water Level: %f Inches\n", v2);     // Display volts
    //waterlevel[13] = "Water Level: ";
    //waterlevelend[10] = (char)v2;
    //waterlevelendend[6] = "inches"; 
    //waterlevelall[29] = waterlevel[13] + waterlevelend[10] + waterlevelendend[6]; 
    return v2;
    //pause(500);                                // Wait 1/10th of a second
}

int main()
{
  adc_init(21,20,19,18);
  while(1)
  {
  float v2 = adc_volts(2);
  float blah = WaterLevel(v2);
  print("Water Level: %d Inches\n", blah);
  pause(2000);
  }
  
  return 0;  
}  

