#include <propeller.h>

static int dht_timeout[32];
static int dht_last_temp, dht_last_humidity, dht_timeout_ignore;

int dht22_getTemp() 
{
  int temp = dht_last_temp;
  temp = temp * 9 / 5 + 320;
  return temp;
}


int dht22_getHumidity() 
{  
  return dht_last_humidity;
}


char dht22_read(int dht_pin) 
{
  if(dht_pin >= 0 && dht_pin < 32) 
  { 
    int dhtpm = (1 << dht_pin);
    if (CNT - dht_timeout[dht_pin] < CLKFREQ/2 && dht_timeout[dht_pin] != 0 && !(dht_timeout_ignore & dhtpm))
      waitcnt(dht_timeout[dht_pin] + CLKFREQ/2);  
    int dhtp[44];
    int dhtc = 0, dhtk = 0, dhts = 0, dhto = 0, dhth = 0, dhtt = 0, dhte = 0;
    OUTA &= ~dhtpm;
    DIRA |= dhtpm;
    waitcnt(CLKFREQ/55 + CNT);  
    DIRA &= ~dhtpm;
    dhto = CNT;
    int dhst = 0;
    for (int j = 0; j < CLKFREQ / 2000; j++) 
    {
      if (!(INA & dhtpm) && !dhst)
        dhst = 1;
      if ((INA & dhtpm) && dhst) 
      {
        dhst = 0;
        dhtp[dhtc] = CNT;
        if (dhtc < 44)
          dhtc++;   
      }
    }
    for (dhtc = 2; dhtc < 18; dhtc++) 
    {
      dhth <<= 1;
      dhth |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    for (dhtc = 18; dhtc < 34; dhtc++) 
    {
      dhtt <<= 1;
      dhtt |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    for (dhtc = 34; dhtc < 42; dhtc++) 
    {
      dhtk <<= 1;
      dhtk |= (dhtp[dhtc] - dhtp[dhtc-1] > CLKFREQ/9800);
    }
    
    dhts = dhtt & 32768;
    dhtt &= 32767;
    dhtt *= (dhts ? -1 : 1);
    dhte = (((dhth >> 8) & 255) + (dhth & 255) + ((dhtt >> 8) & 255) + (dhtt & 255)) & 255;
    dht_last_humidity = dhth;
    dht_last_temp = dhtt;
    dht_timeout[dht_pin] = CNT;
    return dhte == dhtk;
    
  } 
  else
    return 0;    
}
