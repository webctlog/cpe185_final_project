#include "simpletools.h"                      // simpletools header
#include <PropWare.h>
#include <printer.h>
#include <hd44780.h>

#define SCL_Pin               15
#define SDA_Pin               14


#define RTC_Addr              0x68            
                                                  
#define RTC_Read              ((RTC_Addr << 1) | 0x01) 
#define RTC_Write             ((RTC_Addr << 1) & 0xFE) 


#define TEMP_MSB              0x11 
#define TEMP_LSB              0x12 

// Control pins
const PropWare::Port::Mask RS = PropWare::Port::P16;
const PropWare::Port::Mask RW = PropWare::Port::P17;
const PropWare::Port::Mask EN = PropWare::Port::P18;

// Data pins
const PropWare::Port::Mask          FIRST_DATA_PIN = PropWare::Port::P19;
const PropWare::HD44780::Bitmode    BITMODE        = PropWare::HD44780::BM_8;
const PropWare::HD44780::Dimensions DIMENSIONS     = PropWare::HD44780::DIM_16x2;



  i2c *dsBus;                    // i2c bus ID
  int secs = 0; 
  int mins = 0;
  int hrs  = 0;
  int am_pm = 0;
  float temp = 0;
  int day  = 0;
  int date = 0;
  int month = 0;
  int year = 0;




/* Function Declarations*/
int bcd_to_dec(int d);                                
int dec_to_bcd(int d) ;
void DS3231_Write(int address, int reg, int value);    
int DS3231_Read(int address, int reg); 
//float getTemp();
void check_Date_Time();                                                  
void set_Date_Time();
void read_Date_Time();


int main()                                    
{  


 
  dsBus = i2c_open(dsBus, SCL_Pin, SDA_Pin,   0);             // Set up I2C bus, get bus ID
 


  
//menu for setting time  


  while(i2c_busy(dsBus, RTC_Addr));                           // ensure i2c bus not busy
  check_Date_Time();
  
  for (;;)   
  { pause(950);
       
  print("%c", CLS);
  read_Date_Time();
  switch(day){
      case 1  :
         print("Sunday, ");
         break;
      case 2  : 
         print("Monday, ");
         break;
      case 3  : 
         print("Tuesday, ");
         break;
      case 4  : 
         print("Wednesday, ");
         break;
      case 5  : 
         print("Thursday, ");
         break;
      case 6  : 
         print("Friday, ");
         break;
      case 7  : 
         print("Saturday, ");
         break; 
  }
  switch(month){
      case 1  :
         print("January ");
         break;
      case 2  : 
         print("February ");
         break;
      case 3  : 
         print("March ");
         break;
      case 4  : 
         print("April ");
         break;
      case 5  : 
         print("May ");
         break;
      case 6  : 
         print("June ");
         break;
      case 7  :
         print("July ");
         break;
      case 8  : 
         print("August ");
         break;
      case 9  : 
         print("September ");
         break;
      case 10  : 
         print("October ");
         break;
      case 11  : 
         print("November ");
         break;
      case 12  : 
         print("December ");
         break;
  }
    print("%d, 20%d \n", date, year);
    print("Time = %02d:%02d:%02d ", hrs, mins, secs);       // Display result


    switch(am_pm){
      case 0b100 :
         print("AM\n");
         break;
      case 0b101 :
         print("AM\n");
         break;
      case 0b110 : 
         print("PM\n");
         break;
      case 0b111 : 
         print("PM\n");
         break;
    }    


    PropWare::HD44780 lcd;
    lcd.start(FIRST_DATA_PIN, RS, RW, EN, BITMODE, DIMENSIONS);

    // Create a printer for easy, formatted writing to the LCD
    PropWare::Printer lcdPrinter(&lcd);

    // Print to the LCD (exactly 32 characters so that we fill up both lines)
    lcdPrinter.printf("%u %s%d 0x%07X", 123456789, "Hello!", -12345, 0xabcdef);
    
  }        
}




void check_Date_Time()
{
  read_Date_Time();
  if (year < 1){
    print("Setting Date & Time...");
    pause(500); 
    set_Date_Time();
  }
}


void set_Date_Time(int hr_10, int hr_1, int yr_10, int yr_1)
{  


  print("\nEnter year(YY): ");
  scan( " %d", &year);  
  yr_10 = year / 10;
  yr_10 <<= 4;
  yr_1 = year % 10;
  year = yr_10 | yr_1;
  
  print("\nEnter Month(1-12): ");
  scan( " %d", &month);


  print("\nEnter Day(1-7 [Sunday = 1]): ");
  scan( " %d", &day);


  print("\nEnter Date(1-31): ");
  scan( " %d", &date);


  print("\nEnter Hour(1-12): ");
  scan( " %d", &hrs);
  hr_10 = hrs / 10;
  hr_10 <<= 4;
  hr_1 = hrs % 10;
  hrs = hr_10 | hr_1;


  print("\nEnter 1 for AM or 2 for PM: ");
  scan( " %d", &am_pm);


// code for am_pm status
  switch(am_pm)
  {
     case 1:  //AM
     {
      hrs = (0x40 | (0x1F & hrs));
      am_pm = 0b100;
      break;
     }
     case 2:  //PM
     {
      hrs = (0x60 | (0x1F & hrs));
      am_pm = 0b110;
      break;


     } 
   }                 
       
      
  
  print("\nEnter Minute(1-59): ");
  scan( " %d", &mins);
  print("\nEnter Second(1-59): ");
  scan( " %d", &secs);
                                                        
  DS3231_Write(dsBus, 0x06, year);//set year 
  DS3231_Write(dsBus, 0x05, dec_to_bcd(month));//set month 
  DS3231_Write(dsBus, 0x03, dec_to_bcd(day));//set day 
  DS3231_Write(dsBus, 0x04, dec_to_bcd(date));//set date 
  DS3231_Write(dsBus, 0x02, hrs);//set hour 
  DS3231_Write(dsBus, 0x01, dec_to_bcd(mins));//set minutes 
  DS3231_Write(dsBus, 0x00, dec_to_bcd(secs));//set seconds 
  
}  


void read_Date_Time()
{
  secs = bcd_to_dec(DS3231_Read(dsBus, 0x00));
  mins = bcd_to_dec(DS3231_Read(dsBus, 0x01));
  hrs = DS3231_Read(dsBus, 0x02);
  am_pm = hrs;
  am_pm >>= 4;
  hrs = bcd_to_dec(0x1F & hrs); 
  day = bcd_to_dec(DS3231_Read(dsBus, 0x03));
  date = bcd_to_dec(DS3231_Read(dsBus, 0x04));
  month = bcd_to_dec(DS3231_Read(dsBus, 0x05));
  year = bcd_to_dec(DS3231_Read(dsBus, 0x06));


}    




int bcd_to_dec(int d)                
{                                                                                          
         return ((d & 0x0F) + (((d & 0xF0) >> 4) * 10)); 
}                                
                                                              


int dec_to_bcd(int d) 
{ 
         return (((d / 10) << 4) & 0xF0) | ((d % 10) & 0x0F); 
} 




void DS3231_Write(int address, int reg, int value)    
{  
         i2c_start(dsBus);                  
         i2c_writeByte(dsBus, RTC_Write); 
         i2c_writeByte(dsBus, reg); 
         i2c_writeByte(dsBus, value);    
         i2c_stop(dsBus); 
}  


int DS3231_Read(int address, int reg) 
{                                      
         int value = 0; 
         i2c_start(dsBus);                                                      
         i2c_writeByte(dsBus, RTC_Write); 
         i2c_writeByte(dsBus, reg); 
         i2c_start(dsBus);                                                      
         i2c_writeByte(dsBus, RTC_Read); 
         value = i2c_readByte(dsBus, 1);                      
         i2c_stop(dsBus);                  
         return value; 
} 




