#include "adcDCpropab.h"
#include <propeller.h>
#include <math.h>
int main(void)
{
	adc_init(21,20,19,18);
	
}

int Lux()
{
	float voltage;
		voltage = adc_volts(3);
		float volts = 1/voltage;
		float resistance = volts/.000520;
		float lux = 12500000 * pow(resistance,-1.4059);
		return lux;
		pause(250);
}

int temperature()
{
	int c = dht22_read(2);
	int t = dht22_getTemp();
	return t;
	pause(250);
}

int humidity()
{
	int c = dht22_read(2);
	int h = dht22_getHumidity();
	return h;
	pause(250);
}
