#include "simpletools.h"                      // Include simple tools
#include "adcDCpropab.h"                      // Include adcDCpropab

int main()                                    // Main function
{
  float v2;                                   // Voltage variables
  adc_init(21,20,19,18);
  while(1)
  {
    v2 = (adc_volts(2) - 0.75) * 5;                  // Check A/D 2 
    if (v2 < 0)
      v2 = 0;
    putChar(HOME);                             // Cursor -> top-left "home"
    print("Water Level: %f Inches\n", v2);          // Display volts
    pause(100);                                // Wait 1/10th of a second
  }
}