 Weather Station
 A Multifunctional Personal Weather Station


Micaela Vazquez (Author)
California State University Sacramento 
Computer Engineering 
CPE 185 Computer Interfacing
Section 2
Dustin Snedeker (Author)
California State University Sacramento 
Computer Engineering 
CPE 185 Computer Interfacing
Section 2
Travis Anderson (Author)
California State University Sacramento 
Computer Engineering 
CPE 185 Computer Interfacing
Section 2
Andrew Pitcock (Author)
California State University Sacramento
Computer Engineering
CPE 185 Computer Interfacing
Section 2



Abstract�This document will describe and define every aspect of our final project for CPE 185. We decided to create a multifunctional tool that has the functionalities of a simple personal weather station. The weather station has a real time clock,  water level sensor, ambient light sensor, ambient air temperature sensor uses bluetooth to connect remotely to a mobile device.  The introduction will detail our development process and materials. Each paragraph following the introduction will cover each functionality that the weather station is capable of.  
 Introduction 
This project was inspired by the elements of the weather that affect our daily life. Due to Sacramento�s mercurial weather this device would be a helpful tool to anyone that is planning on going outside. Our vision for the device to be placed on the roof of a home in direct sunlight that is not blocked by trees, power lines...etc. To maximize the devices full capabilities, especially the water level meter we suggest placing the device near or in the gutters of the roof so that the water level sensor is in the gutter vertically. In addition to measuring the water levels in a homes roof gutters this device provides the user the day of the week, date, time of day, a greeting based on the time of day, the temperature and humidity of the air, and fire alerts through a bluetooth connection with the users mobile device. To create this tool the team used the following materials: Parallax Propeller Activity Board WX, HiLetgo DHT22 / AM2302 Temperature and Humidity Sensor, Elegoo DS-3231 Real Time Clock Module, Elegoo Water Sensor Module, Elegoo Infrared Flame Sensor, Ambient Light Sensor, and Elegoo Bluetooth Module. Each team member picked a functionality that they would take responsibility for, according to their skills and experience with it. Team Member Micaela Vazquez was responsible for the Real Time Clock and the Ambient Light Sensor Greeting Message. Team Members Dustin Snedeker and Micaela Vazquez worked together to develop the Flame Detection Function on the weather station. Snedeker was also responsible for the Water Level Module. The temperature and humidity measurement capabilities were developed by Andrew Pitcock. Bluetooth functionality to receive information collected from the weather station was made possible by the work of Team Member Travis Anderson. 
 
Real Time Clock
To make the weather station a one-stop-shop for the user we decided it was important to include a clock on the weather station. When information is requested from the station it will be sent to the users mobile device with an accurate time stamp along with the additional requested information. 
DS-3231 Real Time Clock Diagram and 


 DHT 22 Temperature and Humidity Sensor
    The DHT 22 is a sensor that measures the temperature and humidity of a given area. As a weather station temperature and humidity is important for it is the basis of the weather system. The DHT 22 uses a single wire protocol to transmit data. The module has an operating temperature range of -40 - 80 degrees celsius. The module operates in the range of 3.3- 5.0 V range. The temperature reading range is from -35 - 75 degrees celsius with an uncertainty of .5 degrees celsius. The humidity range of the DHT is from 0%-100% RH with an uncertainty of 5% RH. The sensitivity of the module for humidity is .1%RH and temperature it is .1 degrees celsius. The process of interfacing the DHT was quite simple because of its single wire protocol.





Time of Day Ambient Light Sensor 
The Ambient Light Sensor was included to make the device a bit more friendly. When information is requested the bluetooth connected mobile phone will receive a �Good Morning�, �Good Afternoon�, or �Good Night� message before the collected data. These messages are sent based off of how much light is being measured by the light sensor. If lots of light is sensed the �Good Afternoon� message is displayed because around noon is when it is the brightest outside. When no light is sensed the �Good Night� message is displayed since its clear the sun is down and night has begun. When very little light is sensed the �Good Morning� message is sent. The goal of this function is to make the delivered message look more user friendly and give the user an idea of how bright it is outside. This feature requires the device to be in unobstructed light so it is able to take accurate measurements. 



Elegoo Water Level Sensor 
The Elegoo Water Level Sensor uses a series of parallel wires embedded on the outside of the sensor to measure the amount of conductivity of the water it is put into. This sensor uses a single analog output pin to transmit the data to the Parallax Propeller, which is then read from the board and output to the screen. The other pins consist of voltage (5V) and ground. With respect to the weather station, this is put into the gutter system to measure the amount of rainfall in a period of time. The output of the analog pin is a raw voltage value that was then converted into measuring the value in inches. 





Elegoo Infrared Flame Sensor

The Elegoo Infrared Flame Sensor has both analog and digital output. In this case, we looked at the analog output for detecting if a fire is occuring or around the area. Since we are reading analog data, the chip is hooked up to +5V on the propeller board. The chip has a photodiode to detect the Infra-Red Light that is emitted by flames. The module is most sensitive to a flame within an IR wavelength of 760-1100 nanometers with a detection angle of 60�. 





















